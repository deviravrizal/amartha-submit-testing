import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://www.saucedemo.com/')

WebUI.waitForJQueryLoad(5)

WebUI.click(findTestObject('Swag Labs/login'))

WebUI.setText(findTestObject('Swag Labs/username'), 'standard_user')

WebUI.setEncryptedText(findTestObject('Swag Labs/password'), 'qcu24s4901FyWDTwXGr6XA==')

WebUI.click(findTestObject('Swag Labs/button login'))

WebUI.delay(2)

WebUI.click(findTestObject('Swag Labs/button tambah item'))

WebUI.delay(2)

WebUI.click(findTestObject('Swag Labs/cek keranjang'))

WebUI.delay(2)

WebUI.click(findTestObject('Swag Labs/button checkout'))

WebUI.setText(findTestObject('Swag Labs/input first name'), 'Devira')

WebUI.setText(findTestObject('Swag Labs/input last name'), 'Testing')

WebUI.setText(findTestObject('Swag Labs/input postal code'), '12345')

WebUI.delay(3)

WebUI.click(findTestObject('Swag Labs/button continue'))

WebUI.delay(2)

WebUI.click(findTestObject('Swag Labs/button finish'))

WebUI.delay(5)

